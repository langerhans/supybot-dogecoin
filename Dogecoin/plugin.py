###
# Copyright (c) 2013, langerhans
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.ircmsgs as ircmsgs
from bs4 import BeautifulSoup
import re
from urllib2 import urlopen
import urllib2
import time
import json

opener = urllib2.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0')]
urllib2.install_opener(opener)

class Dogecoin(callbacks.Plugin):
    """wow. such irc. much bot. amaze."""
    threaded = True
    lastbtc = 0
    lastbtcsell = 0
    lastbtcbuy = 0
    lastrequestbtc = 0
    cachetime = 30
    cachetimediff = 300
    lastdiff = 0
    lastrequestdiff = 0

    def _grabapi(self, apipaths):
        source = 'http://dogechain.info/chain/Dogecoin'
        url = source + apipaths
        try:
            data = urlopen(url, timeout=5).read()
            return data
        except:
            return

    def _blocks(self):
        data = self._grabapi('/q/getblockcount')
        return data

    def blocks(self, irc, msg, args):
        '''takes no arguments

        Get current block count.'''
        data = self._blocks()
        if data is None or data == '':
            irc.error("Failed to retrieve data. Try again later.")
            return
        irc.reply('Current block is: ' + data)
    blocks = wrap(blocks)

    def _balance(self, addr):
        data = self._grabapi('/q/addressbalance/' + addr)
        return data

    def balance(self, irc, msg, args, addr):
        '''<address>

        Retruns the balance of the address.'''
        data = self._balance(addr)
        if data is None or data == '':
            irc.error("Failed to retrieve data. Try again later.")
            return
        irc.reply('Balance is: ' + data + ' DOGE')
    balance = wrap(balance, ['anything'])

    def _diff(self):
        data = self._grabapi('/q/getdifficulty')
        self.lastdiff = data
        return data

    def diff(self, irc, msg, args):
        '''takes no arguments

        Get current difficulty.'''
        if time.time() - self.lastrequestdiff > self.cachetimediff:
            data = self._diff()
            self.lastrequestdiff = time.time()
        else:
            data = str(self.lastdiff)
        if data is None or data == '':
            irc.error("Failed to retrieve data. Try again later.")
            return
        irc.reply('Current difficulty is: ' + data)
    diff = wrap(diff)

    def _received(self, addr):
        data = self._grabapi('/q/getreceivedbyaddress/' + addr)
        return data

    def received(self, irc, msg, args, addr):
        '''<address>

        Retruns total received amount of the address.'''
        data = self._received(addr)
        if data is None or data == '':
            irc.error("Failed to retrieve data. Try again later.")
            return
        irc.reply('Total received amount: ' + data + ' DOGE')
    received = wrap(received, ['anything'])

    def _sent(self, addr):
        data = self._grabapi('/q/getsentbyaddress/' + addr)
        return data

    def sent(self, irc, msg, args, addr):
        '''<address>

        Retruns total sent amount of the address.'''
        data = self._sent(addr)
        if data is None or data == '':
            irc.error("Failed to retrieve data. Try again later.")
            return
        irc.reply('Total sent amount: ' + data + ' DOGE')
    sent = wrap(sent, ['anything'])

    def _totaldoge(self):
        data = self._grabapi('/q/totalbc')
        return data

    def totaldoge(self, irc, msg, args):
        '''takes no arguments

        Get total amount of DOGE mined.'''
        data = self._totaldoge()
        if data is None or data == '':
            irc.error("Failed to retrieve data. Try again later.")
            return
        irc.reply('Total mined DOGE: ' + data)
    totaldoge = wrap(totaldoge)

    def _cryptsy(self):
        try:
            marketdata = urlopen('http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=132', timeout=5)
            orderbook = urlopen('http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=132', timeout=5)
        except:
            return
        j_market = json.load(marketdata)
        j_orders = json.load(orderbook)
        if j_market['success'] == 1 and j_orders['success'] == 1:
            self.lastbtc = j_market['return']['markets']['DOGE']['lasttradeprice']
            self.lastbtcsell = j_orders['return']['DOGE']['sellorders'][0]['price']
            self.lastbtcbuy = j_orders['return']['DOGE']['buyorders'][0]['price']
            return 'Current rates at Cryptsy: Last: ' + self.lastbtc + 'BTC | Sell: ' + self.lastbtcsell + 'BTC | Buy: ' + self.lastbtcbuy + 'BTC'
        else:
            return

    def market(self, irc, msg, args):
        '''takes no arguments

        Returns current BTC exchange rates at Cryptsy.'''
        if time.time() - self.lastrequestbtc > self.cachetime:
            data = self._cryptsy()
            self.lastrequestbtc = time.time()
        else:
            data = 'Current rates at Cryptsy: Last: ' + self.lastbtc + 'BTC | Sell: ' + self.lastbtcsell + 'BTC | Buy: ' + self.lastbtcbuy + 'BTC'
        if data is None or data == '':
            irc.error("Failed to retrieve data. Maybe Cryptsy is not responding fast enough. Try again later.")
            return
        irc.reply(data)
    market = wrap(market)

    def _mtgox(self, cur, val):
        try:
            ticker = urlopen('http://data.mtgox.com/api/2/BTC' + cur.upper() + '/money/ticker_fast', timeout=5)
        except:
            return
        j_ticker = json.load(ticker)
        if self.lastbtc == 0:
            self._cryptsy()
        if j_ticker['result'] == 'success' and self.lastbtc != 0:
            btc_cur = float(j_ticker['data']['last']['value'])
            doge_btc = val*float(self.lastbtc)
            return str(val) + ' DOGE = ' + str(round(doge_btc*btc_cur, 2)) + ' ' + cur.upper()
        else:
            return

    def convert(self, irc, msg, args, val, cur):
        '''<value> [<currency>]

        Converts DOGE to <currency> or USD if no currency is specified. Available currencies: USD, AUD, CAD, CHF, CNY, DKK, EUR, GBP, HKD, JPY, NZD, PLN, RUB, SEK, SGD, THB, NOK, CZK'''
        if cur == '' or cur is None:
            data = self._mtgox('USD', val)
        else:
            data = self._mtgox(cur, val)
        if data is None or data == '':
            irc.error("Failed to retrieve data. Maybe the APIs are currently unavailable. Try again later.")
            return
        irc.queueMsg(ircmsgs.notice(msg.nick, data))
        irc.noReply()
    convert = wrap(convert, ['float', optional('text')])


Class = Dogecoin

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
