Dogecoin plugin for supybot.

Current command list:

balance <address>
	Shows balance of the supplied address.
blocks
	Shows number of mined blocks.
diff
	Shows current difficulty.
market
	Shows current BTC exchange rates from Cryptsy.
received <address>
	Shows total received DOGE of supplied address.
sent <address>
	Shows total received DOGE of supplied address.
totaldoge
	Shows total amount of mined DOGE.
convert <value> [<currency>]
	Converts DOGE to <currency> or USD if no currency is specified. Available currencies: 
	USD, AUD, CAD, CHF, CNY, DKK, EUR, GBP, HKD, JPY, NZD, PLN, RUB, SEK, SGD, THB, NOK, CZK
